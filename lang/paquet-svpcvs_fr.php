<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/svp.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'svpcvs_description' => 'Ce plugin gère l’installation et la mise à jour des plugins en utilisant Git ou SVN.',
	'svpcvs_slogan' => 'Téléchargement des Plugins par Git ou SVN'
);
